#!/bin/bash

REPLAY_RESOLUTION=1280x720
LIVE_RESOLUTION=854x480
LIVE_AVBR=1024
LIVE_MVBR=1280
REPLAY_AVBR=2048
REPLAY_MVBR=3072
ABITRATE=128
V4LDEVICE=0
FPS=10

if [[ $# -lt 4 ]] ; then
  echo "Not enough arguments supplied" >&2
  echo "You need to pass the PeerTube server name as the " >&2
  echo "first parameter the rtmp port as the second, the" >&2
  echo "streaming key as the third and the v4l2 device as the fourth" >&2
  echo " " >&2
  echo "syntax: " >&2
  echo "  $(basename $0) <peertube-server> <rtmp port> <streaming-key> <v4ldevice>" >&2
  echo " " >&2
  echo "Example :" >&2
  echo "  `basename $0` tube.example.com 1935 69ac2d29-eba4-4d5f-a3a6-6047e6768ffb 0" >&2
  exit 1
fi

URL=rtmp://$1:$2/live/$3

V4LDEVICE=$4

KEYFRAMEINTERVAL=$(echo "2*${FPS}" | bc)

BUFFERSIZE=$(echo "2*${VBITRATE}" | bc)




FILENAME=${1}-cam${V4LDEVICE}
echo " "
echo Will stream to $URL
echo and save a copy of the data file :
echo $FILENAME

# A la ligne xx, utiliser ces lignes pour avoir un son de test généré par ffmpeg : 
# -f lavfi -i aevalsrc="(1-min(1\,mod(floor(5*t)\,50)))*0.1*sin(8*PI*(360-2.5/2)*t)+0.1*sin(2*PI*(360-2.5/2)*t)|(1-min(1\,mod(floor(5*t)\,50)))*0.1*sin(8*PI*(360+2.5/2)*t)+0.1*sin(2*PI*(360+2.5/2)*t):sample_rate=48000" \
# -ar 48000 \
# -channel_layout stereo \
# -c:a aac -b:a ${ABITRATE}k \

# A la ligne xx, utiliser ces lignes pour avoir le son de la caméra : 
# -f alsa -ac 2 -i hw:CARD=CAMERA,DEV=${V4LDEVICE} \
# -c:a copy \

# A la ligne xx, utiliser ces lignes pour avoir le son d'une web radio : 
# -f aac -ac 2 -i http://91.121.159.124:8000/eko-des-garrigues-48k.aac \
# -c:a copy \


ffmpeg \
-loglevel level \
-y \
-f v4l2 -framerate 30 -video_size ${REPLAY_RESOLUTION} -input_format mjpeg -i /dev/video${V4LDEVICE} \
-f aac -ac 2 -i http://91.121.159.124:8000/eko-des-garrigues-48k.aac \
-c:a copy \
-filter_complex "[0:v]drawtext='font=DejaVuSansMono:text=%{pts}: fontcolor=white: fontsize=h/24: box=1: boxcolor=black@0.43: boxborderw=5: x=mod(floor(t/10+1)\,2)*mod(t*(w-text_w)/10\,w-text_w)+mod(floor(t/10)\,2)*(w-text_w-mod(t*(w-text_w)/10\,w-text_w)): y=text_h/2', drawtext='font=DejaVuSansMono:text=%{localtime}: fontcolor=white: fontsize=h/20: box=1: boxcolor=black@0.43: boxborderw=5: x=text_h: y=h-3*text_h/2', split=2[out1][out2]" \
-flags +global_header -preset superfast \
-map '[out1]' -pix_fmt yuv420p -s ${LIVE_RESOLUTION} -c:v libx264 -b:v ${LIVE_AVBR}k -maxrate:v ${LIVE_MVBR}k -g:v ${KEYFRAMEINTERVAL} \
-keyint_min:v ${KEYFRAMEINTERVAL} -r:v ${FPS} -x264-params:v scenecut=0:open_gop=0 -bufsize:v 4096k -profile:v high \
-map 1:a \
-f flv ${URL} \
-map '[out2]' -pix_fmt yuv420p -s ${REPLAY_RESOLUTION} -c:v libx264 -b:v ${LIVE_AVBR}k -maxrate:v ${LIVE_MVBR}k -g:v ${KEYFRAMEINTERVAL} \
-keyint_min:v ${KEYFRAMEINTERVAL} -r:v ${FPS} -x264-params:v scenecut=0:open_gop=0 -bufsize:v 8192k -profile:v high \
-map 1:a \
-f matroska -f segment -segment_time 10:00 -reset_timestamps 1 -strftime 1 "${FILENAME}_%Y-%m-%d_%H-%M-%S.mkv"



# Notes
# Commande ffmpeg utilisée par own_cast
#/usr/bin/ffmpeg -hide_banner -loglevel warning \
#-fflags +genpts \
#-i pipe:0 -map v:0 \
#-c:v:0 libx264 -b:v:0 1008k -maxrate:v:0 1088k -g:v:0 48 -keyint_min:v:0 48 -r:v:0 24 \
#-x264-params:v:0 scenecut=0:open_gop=0 -bufsize:v:0 1088k -profile:v:0 high \
#-map a:0? -c:a:0 copy \
#-preset superfast \
#-var_stream_map v:0,a:0 \
#-f hls -hls_time 2 -hls_list_size 15 -hls_flags program_date_time+independent_segments+omit_endlist \
#-segment_format_options mpegts_flags=mpegts_copyts=1 \
#-tune zerolatency \
#-pix_fmt yuv420p \
#-sc_threshold 0 \
#-master_pl_name stream.m3u8 \
#-hls_segment_filename http://127.0.0.1:45627/%v/stream--KmjKxb4g-%d.ts \
#-max_muxing_queue_size 400 \
#-method PUT http://127.0.0.1:45627/%v/stream.m3u8

