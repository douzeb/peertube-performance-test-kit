#!/bin/bash

if [[ $# -lt 2 ]] ; then
    echo "Not enough arguments supplied" >&2
    echo "you need to pass the peertubeserver name as the fisrt argument" >&2
    echo "and the streaming key as the second argument" >&2
    echo "expl: `basename $0` sub.domain.tld 69ac2d29-eba4-4d5f-a3a6-6047e6768ffb" >&2
    exit 1
fi

./stream_random_noise.sh $1 $2 3600 1280x720 4096 160
